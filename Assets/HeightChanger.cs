﻿using UnityEngine;
using System.Collections;

public class HeightChanger : MonoBehaviour {
	Vector3 cameraPositionVector3;
	// Use this for initialization
	void Start () {
		cameraPositionVector3 = camera.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if ( Input.GetAxisRaw("Height") > 0 || Input.GetAxisRaw("Mouse ScrollWheel") > 0) {
			cameraPositionVector3.y += 0.05f;
			if ( cameraPositionVector3.y > 1.0f ) {
				cameraPositionVector3.y = 1.0f;
			}
		}
		else if ( Input.GetAxisRaw("Height") < 0 || Input.GetAxisRaw("Mouse ScrollWheel") < 0) {
			cameraPositionVector3.y -= 0.05f;
			if ( cameraPositionVector3.y < 0 ) {
				cameraPositionVector3.y = 0;
			}
		}
		camera.transform.localPosition = cameraPositionVector3;
	}
}
