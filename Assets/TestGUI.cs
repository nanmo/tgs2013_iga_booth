﻿using UnityEngine;
using System.Collections;

public class TestGUI : MonoBehaviour {
	public Transform target;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		GUILayout.Label("Indie Game Corner/Area booth test @ Tokyo Game Show 2013");
		GUILayout.Label("Move: WASD");
		GUILayout.Label("Height Up/Down: Up/Down Arrow or Mouse Wheel");
		GUILayout.Label("Current Height: " + camera.transform.position.y.ToString("F") +"meters");
		GUILayout.Label("Distance of You to Booth center:" + Mathf.Sqrt(
			Mathf.Pow( Mathf.Abs(target.position.x - camera.transform.position.x) , 2)
			+ Mathf.Pow( Mathf.Abs(target.position.z - camera.transform.position.z), 2) ) + "meters" );
	}
}